module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.7.2"

  cluster_name                         = var.cluster.name
  cluster_version                      = var.cluster.version
  cluster_endpoint_private_access      = var.cluster.endpoint_private_access
  cluster_endpoint_public_access       = var.cluster.endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster.endpoint_public_access_cidrs

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    ami_type               = var.managed_node_defaults.ami_type
    disk_size              = var.managed_node_defaults.disk_size
    instance_types         = var.managed_node_defaults.instance_types
    vpc_security_group_ids = [module.vpc.default_security_group_id]
  }

  eks_managed_node_groups = {
    green = {
      min_size     = var.managed_node_defaults.min_size
      max_size     = var.managed_node_defaults.max_size
      desired_size = var.managed_node_defaults.desired_size

      instance_types = ["t3.large"]
      capacity_type  = var.managed_node_defaults.capacity_type
    }
  }
}
