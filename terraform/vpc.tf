locals {
  region = var.region
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.12.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs                     = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets         = var.private_subnets
  public_subnets          = var.public_subnets
  map_public_ip_on_launch = var.map_public_ip_on_launch

  enable_nat_gateway = var.enable_nat_gateway
  enable_vpn_gateway = var.enable_vpn_gateway

  enable_flow_log                      = var.vpc_flow_log.enabled
  flow_log_destination_type            = var.vpc_flow_log.destination_type
  create_flow_log_cloudwatch_log_group = var.vpc_flow_log.create_cloudwatch_log_group
  create_flow_log_cloudwatch_iam_role  = var.vpc_flow_log.create_cloudwatch_iam_role
  flow_log_max_aggregation_interval    = var.vpc_flow_log.max_aggregation_interval

  tags = {
    Terraform = "true"
  }
}
