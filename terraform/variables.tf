variable "region" {
  description = "AWS Region in which to create infrastructure"
  default     = "us-east-2"
  type        = string
}

variable "vpc_cidr" {
  description = "CIDR range of the VPC network"
  default     = "10.0.0.0/16"
  type        = string
}

variable "vpc_name" {
  description = "Human-readable name for the VPC created"
  type        = string
}

variable "map_public_ip_on_launch" {
  description = "Boolean to map ip on launch or not"
  default     = false
  type        = bool
}

variable "vpc_flow_log" {
  description = "Object containing rules around the VPC flow log using Cloudwatch"
  type = object({
    enabled                     = bool
    destination_type            = string
    create_cloudwatch_log_group = bool
    create_cloudwatch_iam_role  = bool
    max_aggregation_interval    = number
  })
  default = {
    enabled                     = true
    destination_type            = "cloud-watch-logs"
    create_cloudwatch_log_group = true
    create_cloudwatch_iam_role  = true
    max_aggregation_interval    = 60
  }
}

variable "private_subnets" {
  description = "List of private subnet ranges"
  type        = list(string)
}

variable "public_subnets" {
  description = "List of public subnet ranges"
  type        = list(string)
}

variable "enable_nat_gateway" {
  description = "Module variable to enable NAT gateways in each AZ"
  default     = true
  type        = bool
}

variable "enable_vpn_gateway" {
  description = "Module variable to enable VPN gateway"
  default     = false
  type        = bool
}

variable "enable_dns_hostnames" {
  description = "Enable DNS hostname support in VPC"
  default     = true
  type        = bool
}

variable "enable_dns_support" {
  description = "Enable DNS support as a whole in VPC"
  default     = true
  type        = bool
}

variable "cluster" {
  description = "EKS cluster module settings as object"
  type = object({
    name    = string
    version = string

    endpoint_private_access      = bool
    endpoint_public_access       = bool
    endpoint_public_access_cidrs = list(string)
  })
}

variable "managed_node_defaults" {
  description = "Object containing managed node default settings for EKS cluster"
  type = object({
    ami_type  = string
    disk_size = number

    min_size     = number
    max_size     = number
    desired_size = number

    capacity_type  = string
    instance_types = list(string)
  })
}
