## What This Does
# aws-eks-project

This project expects the following environment variables to be set locally. If they are not set when `terraform` commands are run, you'll see connection errors.

- `export AWS_ACCESS_KEY_ID=ACCESS_KEY_ID`
- `export AWS_SECRET_KEY=SECRET_KEY`

